export class TodoModel {
  static nextId: number = 0;
  static getId(): number {
    return TodoModel.nextId++;
  }

  id: number;
  text: string;
  completed: boolean = false;

  constructor(parameters: TodoModelParams) {
    apply(this, parameters);

    if (this.id == null) {
      this.id = TodoModel.getId();
    }
  }
}

export interface TodoModelParams {
  id?: number;
  text: string;
  completed?: boolean;
}
