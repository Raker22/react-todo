function addGlobal(key: string, value: any): void {
  window[key] = value;
}

function apply (dest: Dictionary, source: Dictionary): any {
  for (const key in source) {
    if (source.hasOwnProperty(key)) {
      dest[key] = source[key];
    }
  }

  return dest;
}

function bind(thisArg: any, method: string) {
  if (thisArg.hasOwnProperty(method)) {
    thisArg[method] = thisArg[method].bind(thisArg);
  }
}

addGlobal('addGlobal', addGlobal);
addGlobal('apply', apply);
