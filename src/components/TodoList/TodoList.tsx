import {Todo} from 'components/Todo/Todo';
import 'components/TodoList/TodoList.css';
import {TodoModel} from 'models/TodoModel';
import * as React from 'react';
import {KeyboardEvent, RefObject} from 'react';

export class TodoList extends React.Component<TodoListProps, TodoListState> {
  state: TodoListState = {
    todos: [],
    filteredTodos: [],
    completedFilter: null
  };

  textInput: RefObject<HTMLInputElement> = React.createRef();

  constructor(props: TodoListProps) {
    super(props);
    this.showAll = this.showAll.bind(this);
    this.showActive = this.showActive.bind(this);
    this.showComplete = this.showComplete.bind(this);
    this.completeAll = this.completeAll.bind(this);
    this.clearCompleted = this.clearCompleted.bind(this);
    this.onInputKeyUp = this.onInputKeyUp.bind(this);
    this.onAddClick = this.onAddClick.bind(this);
    this.onTodoChange = this.onTodoChange.bind(this);

    apply(this.state, props);
  }

  render(): React.ReactNode {
    const todos: JSX.Element[] = [];
    const allComplete: boolean = this.state.todos.every((todo: TodoModel) => {
      return todo.completed;
    });
    const someComplete: boolean = this.state.todos.some((todo: TodoModel) => {
      return todo.completed;
    });
    const completeAllEnabled: boolean = this.state.completedFilter !== true && !allComplete;
    const clearCompletedEnabled: boolean = this.state.completedFilter !== false && someComplete;

    for (const todo of this.filterTodos(this.state.todos, this.state.completedFilter)) {
      todos.push(<div key={todo.id}><Todo todo={todo} onChange={this.onTodoChange} /><button onClick={this.deleteTodo.bind(this, todo.id)}>Delete</button></div>);
    }

    return (
      <div>
        <div>
          <input ref={this.textInput} placeholder="What needs to be done?" onKeyUp={this.onInputKeyUp} />
          <button onClick={this.onAddClick}>Add</button>
        </div>

        <div>
          <span>Show</span>
          <input id="allRadio" type="radio" checked={this.state.completedFilter == null} onChange={this.showAll} />
          <label htmlFor="allRadio">All</label>

          <input id="activeRadio" type="radio" checked={this.state.completedFilter === false} onChange={this.showActive} />
          <label htmlFor="activeRadio">Active</label>

          <input id="completeRadio" type="radio" checked={this.state.completedFilter === true} onChange={this.showComplete} />
          <label htmlFor="completeRadio">Complete</label>
        </div>

        <button disabled={!completeAllEnabled} onClick={this.completeAll}>Mark All as Complete</button>

        {todos}

        {clearCompletedEnabled ? <button onClick={this.clearCompleted}>Clear Completed</button> : null}
      </div>
    );
  }

  showAll(): void {
    this.setState({ completedFilter: null });
  }

  showActive(): void {
    this.setState({ completedFilter: false });
  }

  showComplete(): void {
    this.setState({ completedFilter: true });
  }

  completeAll(): void {
    this.setState((state: TodoListState) => {
      return {
        todos: state.todos.map((todo: TodoModel) => {
          todo.completed = true;
          return todo;
        })
      }
    });
  }

  clearCompleted(): void {
    this.setState((state: TodoListState) => {
      return {
        todos: state.todos.filter((todo: TodoModel) => {
          return !todo.completed;
        })
      }
    });
  }

  onTodoChange(value: TodoModel): void {
    this.setState((state: TodoListState) => {
      return {
        todos: state.todos.map((todo: TodoModel) => {
          return value.id === todo.id ? value : todo;
        })
      };
    })
  }

  deleteTodo(id: number): void {
    this.setState((state: TodoListState) => {
      const todos: TodoModel[] = state.todos.filter((todo: TodoModel) => {
        return todo.id !== id;
      });

      return {
        todos: todos
      }
    })
  }

  filterTodos(todos: TodoModel[], filter: boolean | null): TodoModel[] {
    return filter == null ? todos.slice() : todos.filter((todo: TodoModel) => {
      return todo.completed === filter;
    });
  }

  onInputKeyUp(event: KeyboardEvent<HTMLInputElement>): void {
    if (event.key === 'Enter') {
      this.onAddClick();
    }
  }

  onAddClick(): void {
    if (this.textInput.current != null) {
      const text = this.textInput.current.value.trim();

      if (text.length > 0) {
        this.setState((state: TodoListState) => {
          const todos: TodoModel[] = state.todos.slice();

          todos.push(new TodoModel({ text: text }));

          return {todos: todos};
        });
      }

      this.textInput.current.value = '';
    }
  }
}

export interface TodoListState {
  todos: TodoModel[];
  filteredTodos: TodoModel[];
  completedFilter: boolean | null;
}

export interface TodoListProps {
  todos?: TodoModel[];
  completedFilter?: boolean | null;
}
