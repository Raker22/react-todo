import 'components/App/App.css';
import {TodoList} from 'components/TodoList/TodoList';
import * as React from 'react';

export class App extends React.Component {
  componentDidMount(): void {
    document.title = 'React Todo';
  }

  render(): React.ReactNode {
    return (
      <div>
        <h1>React Todo</h1>

        <TodoList />
      </div>
    );
  }
}
