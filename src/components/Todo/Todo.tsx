import 'components/Todo/Todo.css';
import {TodoModel} from 'models/TodoModel';
import * as React from 'react';
import {ChangeEvent, KeyboardEvent} from 'react';

export class Todo extends React.Component<TodoProps, TodoState> {
  state: TodoState = {
    todo: null!,
    editing: false,
    newText: ''
  };

  onChangeCallback?: ((value: TodoModel) => void) | null;

  constructor(props: TodoProps) {
    super(props);
    this.startEdit = this.startEdit.bind(this);
    this.cancelEdit = this.cancelEdit.bind(this);
    this.finishEdit = this.finishEdit.bind(this);
    this.onTextChange = this.onTextChange.bind(this);
    this.onTextKeyUp = this.onTextKeyUp.bind(this);
    this.onCheck = this.onCheck.bind(this);

    apply(this.state, props);

    this.onChangeCallback = props.onChange;
  }

  render(): React.ReactNode {
    let viewEditBlock: JSX.Element;

    if (this.state.editing) {
      viewEditBlock = <span>
        <input value={this.state.newText} onChange={this.onTextChange} onKeyUp={this.onTextKeyUp} />
        <button onClick={this.finishEdit}>Save</button>
        <button onClick={this.cancelEdit}>Cancel</button>
      </span>
    }
    else {
      viewEditBlock = <span>
        <label htmlFor={`todoCheckbox${this.state.todo.id}`}>{this.state.todo.text}</label>
        <button onClick={this.startEdit}>Edit</button>
      </span>
    }

    return (
      <span>
        <input id={`todoCheckbox${this.state.todo.id}`} type="checkbox" checked={this.state.todo.completed} onChange={this.onCheck} />
        {viewEditBlock}
      </span>
    );
  }

  startEdit(): void {
    this.setState((state: TodoState) => {
      return {
        editing: true,
        newText: state.todo.text
      };
    });
  }

  cancelEdit(): void {
    this.setState({ editing: false });
  }

  finishEdit(): void {
    this.setState((state: TodoState) => {
      const todo: TodoModel = new TodoModel({
        ...state.todo,
        text: state.newText
      });

      this.change(todo);

      return {
        editing: false,
        todo: todo
      };
    });
  }

  onTextChange(event: ChangeEvent<HTMLInputElement>): void {
    this.setState({ newText: event.currentTarget.value });
  }

  onTextKeyUp(event: KeyboardEvent<HTMLInputElement>): void {
    if (event.key === 'Enter') {
      this.finishEdit();
    }
    else if (event.key === 'Escape') {
      this.cancelEdit();
    }
  }

  onCheck(event: ChangeEvent<HTMLInputElement>): void {
    const checked: boolean = event.currentTarget.checked;

    this.setState((state: TodoState) => {
      const todo: TodoModel = new TodoModel({
        ...state.todo,
        completed: checked
      });

      this.change(todo);

      return {
        todo: todo
      };
    });
  }

  change(todo: TodoModel): void {
    if (this.onChangeCallback != null) {
      this.onChangeCallback(todo);
    }
  }
}

export interface TodoState {
  todo: TodoModel;
  editing?: boolean;
  newText: string;
}

export interface TodoProps {
  todo: TodoModel;
  onChange?: ((value: TodoModel) => void) | null;
}
